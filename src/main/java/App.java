import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.methods.CloseableHttpResponse;
import java.sql.*;
import java.util.HashMap;

class DatabaseConnection {
    Connection myConnection;

    public void startConnection(){

        // Please Edit username and password
        // Run the given .sql file
        String dbUrl ="jdbc:mysql://localhost:3306/processline";
        String username = "madnisal";
        String password = "password";
        try{
            this.myConnection = DriverManager.getConnection(dbUrl,username,password);
        }catch(Exception e){
            System.out.println(e);
        }

    }

    public void UpdateData(HashMap<String,int[][]> x,int hour, int date,int month, int year){
        try{
            Statement myStatement = this.myConnection.createStatement();
            myStatement.executeUpdate(" CREATE TABLE IF NOT EXISTS productionLineNames (name varchar(20) , PRIMARY KEY(name))");
            for (String key : x.keySet()){
                System.out.println(" INSERT IGNORE INTO productionLineNames VALUES ("+key+")");
                myStatement.executeUpdate(" INSERT IGNORE INTO productionLineNames VALUES ('"+key+"')");
                myStatement.executeUpdate("CREATE TABLE IF NOT EXISTS "+key+" " +
                        "(Hour int NOT NULL,Date int NOT NULL,Month int NOT NULL, Year int NOT NULL, productionLine int , Error int, Total int, PRIMARY KEY(Hour,Date,Month,Year,productionLine));");
                for (int j=0;j<x.get(key).length;j++){
                    myStatement.executeUpdate("INSERT INTO "+key+" VALUES " +"("+hour+","+date+","+month+","+year+","+j+","+x.get(key)[j][0]+","+x.get(key)[j][1]+")" +
                            " ON DUPLICATE KEY UPDATE Error = Error + "+x.get(key)[j][0]+", Total = Total + "+x.get(key)[j][1]);
                }
            }

        }catch(Exception e){
            System.out.println(e);
        }

    }

}



class Sensor {

    String name;


    public Sensor(String name){
        this.name = name;
    }
    String getName(){
        return this.name;
    }

    void OutputProduct(Product p) throws Exception {
        Random rand = new Random();
        int  probability = rand.nextInt(100);

        //probability of getting an error in process is set to 5%

        if (probability>95) {
            p.Broken = true;
        }

        synchronized (App.lock){
            if (!App.LineDetails.containsKey(p.line.name)){
                int[][] x = {{0,0},{0,0},{0,0},{0,0}};
                App.LineDetails.put(p.line.name,x);
                System.out.println("d");
            }
            if(this.getName().equals("S1")){
                if(p.Broken == true){
                    App.LineDetails.get(p.line.name)[0][0]++;
                }
                App.LineDetails.get(p.line.name)[0][1]++;
            }else if(this.getName().equals("S2")){
                if(p.Broken == true){
                    App.LineDetails.get(p.line.name)[1][0]++;
                }
                App.LineDetails.get(p.line.name)[1][1]++;
            }else if(this.getName().equals("S3")){
                if(p.Broken == true){
                    App.LineDetails.get(p.line.name)[2][0]++;
                }
                App.LineDetails.get(p.line.name)[2][1]++;
            }else if(this.getName().equals("S4")){
                if(p.Broken == true){
                    App.LineDetails.get(p.line.name)[3][0]++;
                }
                App.LineDetails.get(p.line.name)[3][1]++;
            }
        }

        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost("http://localhost:8080");
        httpPost.addHeader("machine",this.getName());
        httpPost.addHeader("productstate",p.Broken+"");
        httpPost.addHeader("line", p.line.name);
        CloseableHttpResponse response2 = httpclient.execute(httpPost);

        try {
            //Uncomment to check wether HTTP POST request was successful
            //System.out.println(response2.getStatusLine());
        } finally {
            response2.close();
        }
    }
}

class ProductionLine implements Runnable{

    Sensor sensor1 = new Sensor("S1");
    Sensor sensor2 = new Sensor("S2");
    Sensor sensor3 = new Sensor("S3");
    Sensor sensor4 = new Sensor("S4");
    String name;



    ArrayList<Sensor> sensorlist = new ArrayList<Sensor>();

    public ProductionLine(String name){
        sensorlist.add(sensor1);
        sensorlist.add(sensor2);
        sensorlist.add(sensor3);
        sensorlist.add(sensor4);
        this.name = name;
    }

    public void run(){
        ExecutorService pool = Executors.newFixedThreadPool(20);
        int count = 0;

        //Number of products wanted in simulation
        while (count < 10000000){
            pool.execute(new Product(this));
            try {
                //sleep for 1000ms/number of products per second
                Thread.sleep(10);

            } catch (InterruptedException ie) { }
            count++;
        }
    }


}

class Product implements Runnable{

    ProductionLine line;

    public Product(ProductionLine p){
        this.line = p;
    }
    boolean Broken = false;


    public void run(){
        Enumeration<Sensor> e = Collections.enumeration(this.line.sensorlist);
        int i = 0;
        Sensor s = null;
        while(this.Broken == false && e.hasMoreElements()){
            s = e.nextElement();
            try{
                s.OutputProduct(this);
            }catch (Exception d){

            }
            i++;
        }
        if(this.Broken == false){
            //System.out.println("product finished");
        }else{
            //System.out.println("product failed at " + s.getName() );
        }
    }

}



public class App {

    public static HashMap<String,int[][]> LineDetails = new HashMap<String, int[][]>();
    public static final Object lock = new Object();

    public static void main(String[] args) throws Exception{

        Thread t1 = new Thread(new ProductionLine("p1"));
        Thread t2 = new Thread(new ProductionLine("p2"));
        final Calendar now = Calendar.getInstance();
        t1.start();
        t2.start();
        t1.join();
        t2.join();
        final DatabaseConnection data = new DatabaseConnection();
        data.startConnection();
        data.UpdateData(LineDetails,now.get(Calendar.HOUR),now.get(Calendar.DATE),now.get(Calendar.MONTH),now.get(Calendar.YEAR));



    }
}

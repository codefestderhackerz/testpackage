# Production Line Simulator

This is a project which simulates a production line manager software which was developed for competing in SLIIT - CodeFest Hackathon

## Getting Started

First clone the project using "git clone https://gitlab.com/codefestderhackerz/testpackage.git"
Then run the project using a prefered IDE
A MYSQL Server should be running at port 3306. Username and password should be specified at lines 19 and 20

## Running the tests

Number of process lines can be simulated by adding new ProcessLine objects in the main method.
(at line 201)
Number of items per second can be simulated by reducing the sleeping time of the product thread.
(at line 150)

## Built With

* [Maven](https://maven.apache.org/) - Dependency Management

## Authors

* **Ranika Madurawe**
* **Dilan Sachintha**
* **Manusha Karunathilake**
* **Sachith Senanayake**